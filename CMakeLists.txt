project(itom)
#trigger update on 2015.12.26@14:09
CMAKE_MINIMUM_REQUIRED(VERSION 2.8)
IF(APPLE)
    SET(CMAKE_OSX_ARCHITECTURES "x86_64")
ENDIF(APPLE)

OPTION(BUILD_TARGET64 "Build for 64 bit target if set to ON or 32 bit if set to OFF." ON)
OPTION(BUILD_UNICODE "Build with unicode charset if set to ON, else multibyte charset." ON)
OPTION(BUILD_WITH_PCL "Build itom with PointCloudLibrary support (pointCloud, polygonMesh, point...)" ON)
SET (ITOM_APP_DIR ${CMAKE_CURRENT_BINARY_DIR} CACHE PATH "base path to itom")
SET (ITOM_SDK_DIR ${CMAKE_CURRENT_BINARY_DIR}/SDK CACHE PATH "base path to itom_sdk")
SET(CMAKE_DEBUG_POSTFIX "d" CACHE STRING "Adds a postfix for debug-built libraries.")
OPTION(BUILD_UNITTEST "Build unittest for itom (including gtest)." OFF)
OPTION(SETUP_ISSCONFIG "Set up batch for inno setup compilation." OFF)
OPTION(BUILD_ITOMLIBS_SHARED "Build dataObject, pointCloud, itomCommonLib and itomCommonQtLib as shared library (default)" ON)
SET_PROPERTY(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug;Release")


# Determined by try-compile from cmake 3.0.2 onwards. Not sure if it's a good idea to set this manually...

#Das mit dem void_p Zeug hatte C.K. mal so reingemacht,
#da es früher mal Probleme bei älteren Compilern damit gab
#(manchmal konnte CMake die Variable CMAKE_SIZEOF_VOID_P nicht korrekt setzen
#    (vor allem wenn man auf einem 64bit OS mit 64 Compiler ein 32bit itom kompilieren wollte.)
#    Ich kenne leider keine Details mehr, aber da es nicht stört und es mal Gründe
#    dafür gab, würde ich das erstmal drin lassen.
if (BUILD_TARGET64)
   set(CMAKE_SIZEOF_VOID_P 8)
else (BUILD_TARGET64)
   set(CMAKE_SIZEOF_VOID_P 4)
endif (BUILD_TARGET64)

SET (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR})

if (WIN32)
    # add /LTCG flag to remove MSVC linker warning in release build
    set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} /LTCG")
    set(CMAKE_STATIC_LINKER_FLAGS_RELEASE "${CMAKE_STATIC_LINKER_FLAGS_RELEASE} /LTCG")
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} /LTCG")
endif (WIN32)

ADD_SUBDIRECTORY(DataObject)

IF(BUILD_WITH_PCL)
    ADD_SUBDIRECTORY(PointCloud)
    SET(ITOM_SDK_LIB_COMPONENTS_STR "dataobject pointcloud addinmanager qpropertyeditor itomCommonLib itomCommonQtLib itomCommonPlotLib itomWidgets itomShapeLib")
ELSE(BUILD_WITH_PCL)
    SET(ITOM_SDK_LIB_COMPONENTS_STR "dataobject addinmanager qpropertyeditor itomCommonLib itomCommonQtLib itomCommonPlotLib itomWidgets itomShapeLib")
ENDIF(BUILD_WITH_PCL)

ADD_SUBDIRECTORY(common)
ADD_SUBDIRECTORY(AddInManager)
ADD_SUBDIRECTORY(shape)
ADD_SUBDIRECTORY(commonQt)
ADD_SUBDIRECTORY(QPropertyEditor)
ADD_SUBDIRECTORY(plot)
ADD_SUBDIRECTORY(itomWidgets)
ADD_SUBDIRECTORY(Qitom)
ADD_SUBDIRECTORY(iconThemes)

IF(BUILD_UNITTEST)
    #put this as last, since gtest is globally changing many compiler flags
    ADD_SUBDIRECTORY(gtest-1.6.0)
    ADD_SUBDIRECTORY(itom_unittests)
ENDIF(BUILD_UNITTEST)
IF(BUILD_ISSCONFIG)
    SET (InnoSetupPath CACHE PATH "base path to inno setup")
ENDIF(BUILD_ISSCONFIG)

#find current itom version from qitom/global.h
FILE(STRINGS ${CMAKE_CURRENT_SOURCE_DIR}/Qitom/global.h ITOM_versionStringFull LIMIT_COUNT 1 REGEX "^#define ITOM_VERSION_STR.*$")
STRING(REGEX MATCH "([0-9]+).([0-9]+).([0-9]+)" ITOM_versionString ${ITOM_versionStringFull}) #searches for Number.Number.Number[.Number] in ITOM_versionStringFull

MESSAGE(STATUS "itom version is: ${ITOM_versionString}. If wrong change it in global.h and reconfigure CMake")
MESSAGE(STATUS "")
MESSAGE(STATUS "CMAKE_CURRENT_BINARY_DIR: ${CMAKE_CURRENT_BINARY_DIR}")
MESSAGE(STATUS "")
################################################################################################################
#CONFIGURE AND PREPARE THE INNO SETUP FILES (require location of Qt, therefore find_package_qt must be called)
include("ItomBuildMacros.cmake")
CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/docs/doxygen/itom_doxygen.dox.in ${CMAKE_CURRENT_BINARY_DIR}/docs/doxygen/itom_doxygen.dox )
CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/docs/userDoc/create_doc.py.in ${CMAKE_CURRENT_BINARY_DIR}/docs/userDoc/create_doc.py )
CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/docs/userDoc/modify_doc.py.in ${CMAKE_CURRENT_BINARY_DIR}/docs/userDoc/modify_doc.py )
CONFIGURE_FILE(${CMAKE_CURRENT_SOURCE_DIR}/docs/pythonSqlDb/PythonAPI_SqlCreator.py ${CMAKE_CURRENT_BINARY_DIR}/docs/pythonSqlDb/PythonAPI_SqlCreator.py )
FILE(COPY ${CMAKE_CURRENT_SOURCE_DIR}/docs/pluginDoc DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/docs)
FILE(COPY ${CMAKE_CURRENT_SOURCE_DIR}/styles DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/licenses DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

################################################################################################################
FIND_PACKAGE_QT(ON Core)
#configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/win32/itom_setup_win32.iss.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win32/itom_setup_win32.iss )
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/win32/itom_setup_win32.iss.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win32/itom_setup_win32.iss )
#configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/win32/itom_setup_win32_common_files.iss.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win32/itom_setup_win32_common_files.iss )
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/common/itom_setup_code.iss.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win32/itom_setup_win32_code.iss )
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/common/start_qt_deployment.bat.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win32/start_qt_deployment.bat )

#configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/win64/itom_setup_win64.iss.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win64/itom_setup_win64.iss )
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/win64/itom_setup_win64.iss.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win64/itom_setup_win64.iss )

#configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/win64/itom_setup_win64_common_files.iss.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win64/itom_setup_win64_common_files.iss )
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/common/itom_setup_code.iss.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win64/itom_setup_win64_code.iss )
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/common/start_qt_deployment.bat.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win64/start_qt_deployment.bat )
IF(SETUP_ISSCONFIG)
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/win32/compile_iss_win32.bat.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win32/compile_iss_win32.bat )
    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/setup/win64/compile_iss_win64.bat.in ${CMAKE_CURRENT_BINARY_DIR}/setup/win64/compile_iss_win64.bat )
ENDIF(SETUP_ISSCONFIG)
################################################################################################################

IF(WIN32)
    SET(CHMSource "${CMAKE_CURRENT_SOURCE_DIR}/docs/itom_doc.chm")
    SET(CHMDest "${CMAKE_CURRENT_BINARY_DIR}/docs")

    IF(EXISTS ${CHMSource})
        IF(EXISTS "${CHMDest}/itom_doc.chm")
            IF(${CHMSource} IS_NEWER_THAN "${CHMDest}/itom_doc.chm")
                message(STATUS "copy ${CHMSource} to ${CHMDest} since it is newer")
                FILE(COPY ${CHMSource} DESTINATION ${CHMDest})
            ENDIF()
        ELSE()
            message(STATUS "copy ${CHMSource} to ${CHMDest}")
            FILE(COPY ${CHMSource} DESTINATION ${CHMDest})
        ENDIF()
    ELSE()
        message(STATUS "${CHMSource} does not exist")
    ENDIF()


    IF(MSVC_VERSION EQUAL 1600)
        IF(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug64.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release64.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ELSE(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug32.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release32.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ENDIF(CMAKE_CL_64)
    ELSEIF(MSVC_VERSION EQUAL 1700)
        IF(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug64_v11.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release64_v11.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ELSE(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug32_v11.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release32_v11.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ENDIF(CMAKE_CL_64)
    ELSEIF(MSVC_VERSION EQUAL 1800)
        IF(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug64_v12.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release64_v12.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ELSE(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug32_v12.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release32_v12.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ENDIF(CMAKE_CL_64)
    ELSEIF(MSVC_VERSION EQUAL 1900)
        IF(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug64_v14.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release64_v14.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ELSE(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug32_v14.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release32_v14.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ENDIF(CMAKE_CL_64)
    ELSEIF((MSVC_VERSION GREATER 1909) AND (MSVC_VERSION LESS 1920))
        IF(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug64_v15.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release64_v15.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ELSE(CMAKE_CL_64)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_debug32_v15.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_debug.bat COPYONLY)
            configure_file(${CMAKE_CURRENT_SOURCE_DIR}/compile_release32_v15.bat ${CMAKE_CURRENT_BINARY_DIR}/compile_release.bat COPYONLY)
        ENDIF(CMAKE_CL_64)
    ENDIF(MSVC_VERSION EQUAL 1600)
ENDIF(WIN32)

#ADD a batch file for itom-release
CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/COPYING.txt ${CMAKE_CURRENT_BINARY_DIR}/COPYING.txt )

CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/itom_sdk.cmake.in ${CMAKE_CURRENT_BINARY_DIR}/SDK/itom_sdk.cmake )
