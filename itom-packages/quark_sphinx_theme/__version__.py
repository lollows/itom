# -*- coding: utf-8 -*-
# This file is part of quark-sphinx-theme.
# Copyright (c) 2016 Felix Krull <f_krull@gmx.de>
# Released under the terms of the BSD license; see LICENSE.

__version_info__ = (0, 2, 1)
__version__ = '.'.join(map(str, __version_info__))
